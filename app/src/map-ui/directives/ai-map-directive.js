app.directive('aiMap', function aiMap(){
	return {
      restrict: 'AE',
      replace: true,
	  scope: {},
	  templateUrl: 'src/map-ui/views/ai-map.html',
	  controller: 'aiMapController'
  };
});
