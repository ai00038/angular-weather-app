app.controller('aiOmniSearchController', ['$scope', '$rootScope','aiOmniSearchService', function aiOmniSearchController($scope, $rootScope, aiOmniSearchService){
	
	var _searchTerm = null;
	
	$scope.searchResults = null;
	
	$scope.searchTermAccessor = function searchTermAccessor(val){
		return arguments.length ? (_searchTerm = val) : _searchTerm;
	}
	
	$scope.evaluateSearchTerm = function evaluateSearchTerm(){
		$scope.searchResults = aiOmniSearchService.evaluateSearchTerm(_searchTerm);// this bit works
		// fire event upwards
		$scope.$emit($scope.searchResults.inputType, $scope.searchResults);

		// firing an event downwards
		$scope.$broadcast($scope.searchResults.inputType, $scope.searchResults);
		
	};
	
	$scope.clearSearch = function clearSearch(){
		_searchTerm = null;
		$scope.searchResults = null;
	};
	
}]);
