app.service('aiOmniSearchService', [ 'aiCityIdRecogService', function aiOmniSearchService(aiCityIdRecogService){

	var searchConstants = {
		LAT_LONG: 'LAT_LONG',
		ZIPCODE: 'ZIPCODE',
		CITY_NAME: 'CITY_NAME',
		CITY_NAME_COUNTRY: 'CITY_NAME_COUNTRY',
		CITY_ID: 'CITY_ID'
	};
	
	/**
		will determine type of input that our searchTerm is,
		returns an object with a cleaner input, ready to send to our api and the type of input it is (of which we probably need omni-search constants)
		
		for now expected inputs are:
		* city Name [+ countrycode] (Strings only)
		* city id (integers only)
		* zip code (one String is presumed to be country code (a-zA-z string, 2 chars), other string is "zip code" delimited by comma... )
		* latlong (two real numbers, delimited by comma, first number assumed to be lat, other number assumed to be long)
		* anything else is so far unqualified...
		
		@ return {object} -> (object, string) [search params, input type]
	*/
	function evaluateSearchTerm(searchTerm){//Check City id for now...
		
		//----------------- BEGIN recognizing inputs
		
		var trimmed = searchTerm.trim(); // all trailing and leading whitespace removed
		
		var tokens = trimmed.split(" ");
		
		var regex = /[0-9]{6,}/;
		
		var _cityId = 0;
		
		if(regex.exec(tokens[0])){
			_cityId = parseInt(tokens[0]);
		}
		
		//-------------END
		
		var _inputType = searchConstants.CITY_ID;
		var _params = {
			cityId : _cityId
		};
		
		return {
			inputType : _inputType,
			params : _params
		}
	}
	
	return {
		evaluateSearchTerm : evaluateSearchTerm
	}

}]);
