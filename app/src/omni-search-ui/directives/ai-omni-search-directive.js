app.directive('aiOmniSearch', function aiOmniSearch(){
	return {
      restrict: 'AE',
      replace: true,
	  scope: {},
	  templateUrl: 'src/omni-search-ui/views/ai-omni-search.html',
	  controller: 'aiOmniSearchController'
  };
});
