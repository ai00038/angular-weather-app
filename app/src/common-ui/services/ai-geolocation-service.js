app.service('aiGeolocationService', [function aiGeolocationService(){
	//find a way to get html5 Geolocation here - could place this in common-ui as both map controller AND weather-data controller will rely on this service
	
	var _currentLatLong = null;
	
	function success(position) {
		var latitude  = position.coords.latitude;
		var longitude = position.coords.longitude;
		_currentLatLong = {
			lat : latitude,
			lon : longitude
		};
	};

	function error() {
		console.log('Unable to retrieve your location');
	};
	  
	function init(){
	  if (!navigator.geolocation){
		console.log('Geolocation is not supported by your browser');
	  }
	  navigator.geolocation.getCurrentPosition(success, error);
	}
	
	return {
		init : init,
		currentLatLong : _currentLatLong
	}
	
}]);
