app.controller('aiWeatherDataController', ['$scope', '$rootScope', 'aiWeatherDataService', 'aiGeolocationService', function aiWeatherDataController($scope, $rootScope, aiWeatherDataService, aiGeolocationService){
	//would be nice to get current location with geolocation and then set default weather info using the latlong
	
	/*
		aiGeolocationService.init();
		
		$scope.currentLocation = aiGeolocationService.currentLatLong;// can get current weather data by latlong
		
		console.log("lat ## "+$scope.currentLocation.lat);
		console.log("long ## " + $scope.currentLocation.lon);
	*/
	$scope.currentWeatherData = null;
	
	function setDataByCityName(cityName,countryCode){
		$scope.currentWeatherData = aiWeatherDataService.getDataByCityName(cityName,countryCode);
	}
	
	function setDataByLatLong(latLong){
		$scope.currentWeatherData = aiWeatherDataService.getDataByLatLong(latLong);
	}
	
	function setDataByZipCode(zip,countryCode){
		$scope.currentWeatherData = aiWeatherDataService.getDataByZipCode(zip,countryCode);
	}
	
	function setDataByCityId(cityId){
		$scope.currentWeatherData = aiWeatherDataService.getDataByCityId(cityId);
	}
	
	function clearWeatherData(){
		$scope.currentWeatherData = null;
	}
	
	$scope.setDataByCityName = setDataByCityName;
	$scope.setDataByLatLong = setDataByLatLong;
	$scope.setDataByZipCode =  setDataByZipCode;
	$scope.setDataByCityId = setDataByCityId;
	$scope.clearWeatherData = clearWeatherData;
	
	$rootScope.$on('CITY_ID', function (event, data) {
		$scope.setDataByCityId(data.params.cityId);
	});
	
	$rootScope.$on('LAT_LONG', function (event, data) {
		$scope.setDataByLatLong(data.params.latLong);
	});
	
	$rootScope.$on('CITY_NAME', function (event, data) {
		$scope.setDataByCityName(data.params.cityName);
	});
	
	$rootScope.$on('CITY_NAME_COUNTRY', function (event, data) {
		$scope.setDataByCityName(data.params.cityName,data.params.countryCode);
	});
	
	$rootScope.$on('ZIPCODE', function (event, data) {
		$scope.setDataByZipCode(data.params.zip, data.params.countryCode);
	});
	
	
}]);
