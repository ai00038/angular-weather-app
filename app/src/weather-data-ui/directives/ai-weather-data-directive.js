app.directive('aiWeatherData', function aiWeatherData(){
	return {
      restrict: 'AE',
      replace: true,
	  scope: {},
	  templateUrl: 'src/weather-data-ui/views/ai-weather-data.html',
	  controller: 'aiWeatherDataController'
  };
});
