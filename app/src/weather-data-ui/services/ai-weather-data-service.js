/**
	Purpose of this Service is to Get current weather Data of an area using either: cityName, cityId, latLong or zipCode
*/
app.service('aiWeatherDataService', ['$resource', function aiWeatherDataService($resource){//use openweather map to get data based on valid inputs

	var baseUrl = "http://api.openweathermap.org/data/2.5/weather",
		callbackFormat = { callback: "JSON_CALLBACK" },
		getMethod = { get: { method: "JSONP" }},
		apiKey = '75ab4a21554c3eea638f0493fc501db1';
		
	var weatherAPI = $resource(baseUrl, callbackFormat, getMethod);
		
	/**
		Gets Data by City Name
		@ param -- cityName {String}
	*/
	function getDataByCityName(cityName,countryCode) {
		if(countryCode){
			return weatherAPI.get({ q: cityName + "," + countryCode, APPID: apiKey })
		}
		return weatherAPI.get({ q: cityName, APPID: apiKey });
	};

	/**
		Gets Data by City Id
		@ param -- cityId {Number}
	*/
	function getDataByCityId(cityId) {
		return weatherAPI.get({ id: cityId, APPID: apiKey });
	};
	
	/**
		Gets Data by LatLong
		@ param -- LatLong {Number, Number}
	*/
	function getDataByLatLong(latLong) {
		return weatherAPI.get({ lat: latLong.lat, lon:latLong.lon, APPID: apiKey }); 
	};
	
	/**
		Gets Data by Zip Code
		@ param -- zipCode (zip,countrycode) {int}{String}
	*/
	function getDataByZipCode(zipExp,countryCode) {
		var zipCode = zipExp + "," +countryCode;
		return weatherAPI.get({ zip: zipCode, APPID: apiKey }); 
	};
	
	return {
		getDataByCityName : getDataByCityName,
		getDataByCityId : getDataByCityId,
		getDataByLatLong : getDataByLatLong,
		getDataByZipCode : getDataByZipCode
	}

}]);
